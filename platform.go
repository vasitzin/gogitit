package main

type Platform struct {
	Name string
	Url  string
}

var GitlabPlatform = Platform{"gitlab", ""}
var GithubPlatform = Platform{"github", ""}
