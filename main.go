package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"
)

const rgxField = `((\\"|[^"])*)`

var rgxGet = regexp.MustCompile(`"` + rgxField + `"`)

func main() {
	cfghome := os.Getenv("XDG_CONFIG_HOME")
	if cfghome == "" {
		log.Fatalln(`Couldn't retrieve 'XDG_CONFIG_HOME' variable from your environment!`)
	}

	listPkgsFlag := flag.Bool("list", false, "list packages")
	forcePkgFlag := flag.String("force", "", "force download package")
	flag.Parse()

	userjsonPath := cfghome + "/gogitit/package.json"
	timestampjsonPath := cfghome + "/gogitit/timestamp.json"
	var packages []Package
	var timestamps []Timestamp

	// parse the user packages
	userjsonData, err := os.ReadFile(userjsonPath)
	if err != nil {
		log.Fatalln(err)
	}
	err = json.Unmarshal(userjsonData, &packages)
	if err != nil {
		log.Fatalln(err)
	}

	// if the list flag was specified print packages and exit
	if *listPkgsFlag {
		listPackages(packages)
		os.Exit(0)
	}

	// if the force flag was specified keep only specified package in array
	if *forcePkgFlag != "" {
		var emptyarray []Package
		for _, p := range packages {
			if p.Title == *forcePkgFlag {
				emptyarray = append(emptyarray, p)
				packages = emptyarray
				break
			}
		}
		if len(emptyarray) == 0 {
			log.Fatalln(`Specified package title`, *forcePkgFlag, ` not in your user package.json`)
		}
	}

	// parse the package timestamps
	timestampjsonData, err := os.ReadFile(timestampjsonPath)
	if err != nil {
		log.Fatalln(err)
	}
	err = json.Unmarshal(timestampjsonData, &timestamps)
	if err != nil {
		log.Fatalln(err)
	}

	for _, p := range packages {
		j := -1
		var t Timestamp
		for index := range timestamps {
			if timestamps[index].Title == p.Title {
				j = index
				t = timestamps[index]
				if *forcePkgFlag != "" {
					t.Last = 0
				}
				break
			}
		}
		if j == -1 {
			var newts Timestamp
			newts.Title = p.Title
			newts.Last = 0
			timestamps = append(timestamps, newts)
			j = len(timestamps) - 1
		}

		resTimestamp, err := syncRelease(p, t)
		if err != nil {
			fmt.Println(err)
			continue
		} else {
			timestamps[j].Last = resTimestamp
		}
	}

	// parse, format and write new timestamps to file and exit
	timestampjsonData, err = json.MarshalIndent(timestamps, "", "  ")
	if err != nil {
		log.Fatalln(err)
	}
	json.Unmarshal(timestampjsonData, &timestamps)
	timestampjsonFile, err := os.OpenFile(timestampjsonPath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	defer timestampjsonFile.Close()
	if err != nil {
		log.Fatalln(err)
	}
	_, err = timestampjsonFile.Write(timestampjsonData)
	if err != nil {
		log.Fatalln(err)
	}
}

// syncRelease does the parsing and the downloading thingy.
func syncRelease(pkg Package, timestamp Timestamp) (int64, error) {
	rgxTime := regexp.MustCompile(`"` + pkg.KeyTime + `":"` + rgxField + `"`)
	rgxLink := regexp.MustCompile(`"` + pkg.KeyLink + `":"` + rgxField + pkg.RgxLink + `"`)
	c, cancel := context.WithCancel(context.Background())
	go progress(pkg.Title, "listening", c)
	res, err := http.Get(pkg.Release)
	cancel()
	if err != nil {
		fmt.Println(`On Release: `, pkg.Release, ` Error: `, err)
		return -1, err
	}
	defer res.Body.Close()
	resBytes, _ := io.ReadAll(res.Body)
	resString := string(resBytes)

	resTimeString := rgxGet.FindAllString(rgxTime.FindString(resString), 2)[1]
	resTimeString = strings.Trim(resTimeString, `"`)
	resTime, _ := time.Parse(time.RFC3339, resTimeString)
	resTimestamp := resTime.Unix()

	resLink := rgxGet.FindAllString(rgxLink.FindString(resString), 2)[1]
	resLink = strings.Trim(resLink, `"`)

	err = pkg.sync(timestamp.Last, resTimestamp, resLink)
	if err != nil {
		return -1, err
	}
	return resTimestamp, nil
}
