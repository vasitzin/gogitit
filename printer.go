package main

import (
	"context"
	"fmt"
	"time"
)

const strUpErase = "\033[1A\033[K"
const strClear = "\033[0m"
const strGreen = "\033[1;92m"
const strRed = "\033[1;91m"
const strCyan = "\033[1;96m"
const strGreenCheck = "\033[1;92m\u2714\033[0m"
const strRedX = "\033[1;91m\u274c\033[0m"
const strCyanDot = "\033[1;96m\u25CF\033[0m"

func progress(title string, stage string, c context.Context) {
	fmt.Print("\n")
	for {
		fmt.Print(strUpErase)
		fmt.Print(title + ": " + stage)
		for i := 0; i < 3; i++ {
			select {
			case <-c.Done():
				return
			default:
				{
					fmt.Print(".")
					time.Sleep(666 * time.Millisecond)
				}
			}
		}
		fmt.Print("\n")
	}
}

func listPackages(packages []Package) {
	fmt.Print("\n")
	for _, p := range packages {
		fmt.Println(strCyan + p.Title + strClear)
		fmt.Println(`├── ` + p.Release)
		fmt.Print(`└── ` + p.Directory + "\n\n")
	}
}
