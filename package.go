package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

type Package struct {
	Title     string
	Release   string
	KeyTime   string
	KeyLink   string
	RgxLink   string
	Extract   string
	Directory string
}

type Timestamp struct {
	Title string
	Last  int64
}

func (p Package) sync(localTimestamp int64, remoteTimestamp int64, link string) error {
	var title, filename, filepath string
	{
		segments := strings.Split(link, "/")    // split the link
		filename = segments[len(segments)-1]    // grab file name
		filepath = p.Directory + "/" + filename // string file path
		if p.Title != "" {
			title = p.Title
		} else {
			title = filename
		}
	}

	if remoteTimestamp <= localTimestamp {
		fmt.Print("\n" + strUpErase + title + ": up-to-date " + strCyanDot + "\n")
		return nil
	}

	err := actionDownload(title, filepath, link)
	if err != nil {
		return err
	}

	if p.Extract != "" {
		err := actionExtract(title, p.Directory, filepath, p.Extract)
		if err != nil {
			return err
		}
	}

	fmt.Print("\n" + strUpErase + title + ": updated " + strGreenCheck + "\n")
	return nil
}

func actionDownload(title string, filepath string, link string) error {
	c, cancel := context.WithCancel(context.Background())
	go progress(title, "downloading", c)
	outputFile, err := os.Create(filepath)
	if err != nil {
		cancel()
		return err
	}
	defer outputFile.Close()
	downloadReader, err := http.Get(link)
	if err != nil {
		cancel()
		return err
	}
	defer downloadReader.Body.Close()
	_, err = io.Copy(outputFile, downloadReader.Body)
	cancel()
	if err != nil {
		fmt.Print("\n" + strUpErase + title + ": download failed " + strRedX + "\n")
		return err
	}
	return nil
}

func actionExtract(title string, directory string, filepath string, cmd string) error {
	c, cancel := context.WithCancel(context.Background())
	go progress(title, "extracting", c)
	cmd = strings.ReplaceAll(cmd, "%f", filepath)
	cmd = strings.ReplaceAll(cmd, "%d", directory)
	extract := exec.Command("/bin/sh", "-c", cmd)
	err := extract.Run()
	cancel()
	if err != nil {
		fmt.Print("\n" + strUpErase + title + ": extraction failed " + strRedX + "\n")
		return err
	}
	err = os.Remove(filepath)
	if err != nil {
		fmt.Print("\n" + strUpErase + title + ": cleanup failed " + strRedX + "\n")
		return err
	}
	return nil
}
