# Go Git It

This is a utility that kind of works like traditional terminal based package managers,
but instead of using a system of repositories with software packaged in a certain format
it is meant to take advantage and work with the "Release" feature of the major Git based software project hosting services.

You will need to create a directory in your `~/.config` named gogitit and a json file named package.json
(`~/.config/gogitit/package.json`
*is manually edited by the user*).

Here's an example of a json file that contains only the release of the Proton GE project on github:
```json
[
    {
        "Title": "GloriousEggroll/proton",
        "Release": "https://api.github.com/repos/GloriousEggroll/proton-ge-custom/releases/latest",
        "KeyTime": "published_at",
        "KeyLink": "browser_download_url",
        "RgxLink": "\\.tar\\.gz",
        "Extract": "tar xzf '%f' -C '%d'",
        "Directory": "/home/user/.steam/root/compatibilitytools.d"
    }
]
```
